var mocha = require('mocha')
var chai = require('chai')
var chaiHttpi = require('chai-http')

var server = require('../server') //Con esto carga el fichero server de mi aplicación, esto levanta la application

var should = chai.should()

chai.use(chaiHttpi) // configuar chai con modulo http

//definimos la suit de pruebas
//Esto es una prueba para comprobar que funciona google
describe('Test de conectividad de Google', () =>{  // () => es una notacion que indica que es una function
  //Ahora se definen la pruebas
  it ('Google funciona',(done) => {
    chai.request('http://www.Google.es')
      .get('/')
      .end((err,res) => {
        //console.log(res)
        res.should.have.status(200)
        done()
      })
  })
})

//Prueba de conectividad de la application
describe('Test de conecividad de la API', () =>{
  //Ahora se definen la pruebas
  it ('Raiz ok',(done) => {
    chai.request('http://localhost:3001')
      .get('/apitechu/v1')
      .end((err,res) => {
        //console.log(res)
        res.should.have.status(200)
        res.body.mensaje.should.be.eql("Bienvenido a mi API") //be.eql porque es una propiedad del JSON

        done()
      })
  })
  it ('lista de usuarios',(done) => {
    chai.request('http://localhost:3001')
    .get('/apitechu/v1/usuarios')
    .end((err,res) => {
      res.should.have.status(200)
      res.body.should.be.a('array')
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('email') //Valida que todos los datos tienen esta propiedades
        res.body[i].should.have.property('password')
        //res.body[i].should.have.property('tururu')
      }
      done()
    })

  })
})

//Prueba que de que todos los elementos tienen email y password
