var usuarios = require ('./usuarios.json')

console.log("Hola mundo")


////////GET ///////

app.get('/apitechu/v1',function(req,res){
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios',function(req,res){
  //res.sendfile('usuarios.json')
  res.send(usuarios)
})


/////////POST/////////
// Añade un nuevo dato //

app.post('/apitechu/v1/usuarios',function(req,res){
  var nuevo ={"first_name":req.headers.first_name,
              "country":req.headers.country
}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios) // Convierte el JSON en cadena de caracteres

  fs.writeFile("./usuarios-2.json",datos,"utf8",function(err){

    if (err)
    {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }

  }) //Escribe en el fiechero el resultado

  res.send("Alta ok")

})


/////////////////Usando el body
app.post('/apitechu/v2/usuarios',function(req,res){

  var nuevo = req.bodyParser

  usuarios.push(nuevo)
  //console.log(req.headers)
  const datos = JSON.stringify(usuarios) // Convierte el JSON en cadena de caracteres

  fs.writeFile("./usuarios-2.json",datos,"utf8",function(err){

    if (err)
    {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }

  }) //Escribe en el fiechero el resultado

  res.send("Alta ok")

})





//////// DELETE ///////
// Borra un datos //

app.delete('/apitechu/v1/usuarios/:id',function(req,res){
  usuarios.splice(req.params.id-1,1)
  res.send("usuario borrado")
})


app.post('/apitechu/v1/mostruo/:p1/:p2',function(req,res){

  console.log("parametros")
  console.log(req.params)

  console.log("quiery string")
  console.log(req.query)

  console.log("headers")
  console.log(req.headers)

  console.log("body")
  console.log(req.body)

})
//            ^
//            |
//            |
//Llamada en postman http://localhost:3000/apitechu/v1/mostruo/pedro/felip?extra=coffe
//Resultado
/*
parametros
{ p1: 'pedro', p2: 'felip' }
quiery string
{ extra: 'coffe' }
headers
{ host: 'localhost:3000',
  connection: 'keep-alive',
  'content-length': '32',
  pais: 'espana',
  'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.140 Chrome/64.0.3282.140 Safari/537.36',
  'cache-control': 'no-cache',
  origin: 'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop',
  'postman-token': 'b50294ca-fd1e-1e71-3996-c1c9b60c7e24',
  'content-type': 'application/json',
  accept: '*',
  'accept-encoding': 'gzip, deflate, br',
  'accept-language': 'es-ES,es;q=0.9' }
body
undefined
para evitar esto hay que poner
app.use(bodyParser.json())

con esto saldrá

{ saludos: 'hola desde atom' }


*/




/////////POST/////////
// login //

app.post('/apitechu/v3/usuarios/login/',function(req,res){

  var password = req.headers.password
  var email=req.headers.email

  console.log(password)
  console.log(email)

  var idusuario = 0

   for (var i = 0; i < usuarios.length; i++) {
     if( usuarios[i].email == email && usuarios[i].password == password)
     {
         idusuario = usuarios[i].id
         usuarios[i].logged = true
         usuarios.push()
         break
     }
   }
   if (idusuario != 0)
   {
     res.send({"encontrado":"si","id":idusuario})
   }else {
     res.send({"encontrado":"no"})
   }

})

/// hago get con la v3
app.get('/apitechu/v3/usuarios',function(req,res){
  //res.sendfile('usuarios.json')
  res.send(usuarios)
})

/////////POST/////////
// logout //

app.post('/apitechu/v3/usuarios/logout/',function(req,res){

var id = req.headers.id
var logado = false
console.log(id)
for (var i = 0; i < usuarios.length; i++) {
  if( (usuarios[i].id == id) &&  (usuarios[i].logged == true))
  {
      console.log(usuarios[i].id)
      usuarios[i].logged = false
      usuarios.push()
      res.send({"logout":"si"})
      logado = true
      break
  }
}

if (!logado)
{res.send({"logout":"no encontrado"})}



})
