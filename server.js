var express = require('express')
var bodyParser = require('body-parser')
var nodemailer = require('nodemailer')
var cors = require('cors')



// Código Nodejs
/*
//Con esta configutación debería funciona en https
var fs = require('fs') // módulo fs
var https = require('https') // módulo https
var requestJson = require('request-json') // módulo request-json// Configuración del servidor

var port = process.env.PORT || 3000 // Definimos el puerto
var app = express()

https.createServer({
   key: fs.readFileSync('./borradorproyecto.key'),
   cert: fs.readFileSync('./borradorproyecto.crt')
}, app).listen(port, function() {
   console.log("My https server listening on port " + port + "...");
});
*/

//Con esta configutación debería funcionar tanto en http como https

var fs = require('fs');
var http = require('http');
var https = require('https');
var requestJson = require('request-json') // módulo request-json// Configuración del servidor
var privateKey  = fs.readFileSync('./borradorproyecto.key', 'utf8');
var certificate = fs.readFileSync('./borradorproyecto.crt', 'utf8');


var credentials = {key: privateKey, cert: certificate};
var express = require('express');
var app = express();

// your express configuration here

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(3001);
httpsServer.listen(3000);

console.log("My https server listening on port " + 3000 + "...");

app.use(bodyParser.json()) // para leer los body los lea en formato json
app.use(cors())

/* Con esta configuracion funciona en http
var app = express()
var port = process.env.PORT || 3000
var fs = require('fs')
app.listen(port)
console.log("Api escuchabdo en puerto " + port)
var requestJson = require('request-json')
app.use(bodyParser.json()) // para leer los body los lea en formato json
app.use(cors())
*/


var usuarios = require ('./usuarios.json')

//Acceder a Mlab para que te traiga la lista de usuarios


var urlMlabRaiz = "https://mlab.com/api/1/databases/bdbancopfm/collections"

var  apikey ="apiKey=t0BFQGkvLMUdr8pP7knZLuDCSpYiBXlZ" // extraida de la web mlab, en la parte de usuario

var clientMlbab = null

////////// Inicio Con Mongo  /////////////////



//Funcion auxiliar que retorna el último id disponble en una lista de movimietos
//la esxtrutura es {"id":1,"fecha":"2017/05/18","importe":-188.43,"moneda":"XOF"}
function ultimoIDMovimietos (listademovimietos)
{
  //Esto es una prueba
  var ultimoMvto = 1
  for (var i = 0; i < listademovimietos.length; i++) {
    if (listademovimietos[i].id>ultimoMvto)
    {
      ultimoMvto = listademovimietos[i].id
    }
  }
  return ultimoMvto
}

//Funcion auxiliar que retorna el último id disponble en una lista de movimietos
//la esxtrutura es {"id":1,"fecha":"2017/05/18","importe":-188.43,"moneda":"XOF"}
function ultimoIDusuario (listaID)
{
  console.log("entro en ultimo id");
  var ultimoId = 1
  console.log("la longitud " + listaID.length);
  for (var i = 0; i < listaID.length; i++) {
    if (listaID[i].id>ultimoId)
    {
      ultimoId = listaID[i].id
    }
  }

  return ultimoId
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//Manda email a un usuario
// http://localhost:3000//apitechu/v5/mandacorreo
// IN --> (headers) "to":"popop@gmail.com" , "subject": "Correo de movimientos", "html":"Composición html del correo"
//
app.get('/apitechu/v5/mandacorreo', function(req,res){

var torec = req.headers.to
var subjectrec = req.headers.subject
var htmlrec = req.headers.html

console.log("To: " + torec );
console.log("El asunto: " + subjectrec);
console.log("El html: " + htmlrec);


  var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'pietrobank@gmail.com',
            pass: 'pietrobank1'
        }
    })

  // Definimos el email
  var mailOptions = {
      from: 'pietrobank@gmail.com',
      to: torec,
      subject: subjectrec,
      text: 'Contenido del email',
      html: htmlrec
  };

  // Enviamos el email
  transporter.sendMail(mailOptions, function(error, info){
    console.log("intento mandar");
      if (error){
          console.log(error);
          res.status(404).send('faltan datos')
      } else {
          console.log("Email sent")
          res.send({"enviado": info })

      }
  })

})

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//Retorna ultimo ID de cliente valido
// http://localhost:3000/apitechu/v5/ultimoId
app.get('/apitechu/v5/ultimoId', function(req,res){
  console.log("Entro")
  var ultimoId = 1
  var fitroNombreAp = 'f={"_id":0,"id":1}'
  //clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+fitroNombreAp + "&"+ apikey)
  var clientMlbab2 = requestJson.createClient(urlMlabRaiz+ "/usuarios?" + "&"+ apikey)
  console.log("La llamada: " +  urlMlabRaiz+ "/usuarios?"+fitroNombreAp + "&"+ apikey)
  clientMlbab2.get('',function(err, resM, body){
      if (!err) {
        console.log("la longitud aquí " + body.length);
        for (var i = 0; i < body.length; i++) {
          if (body[i].id>ultimoId)
          {
            ultimoId = body[i].id
          }
        }

        var retorno = ultimoId + 1
        console.log("Aquí el ultimo id retorno: " + retorno)

        res.send({"idUsuarioNuevo": retorno })


      }else {
        res.send({"idUsuarioNuevo": ultimoId })
      }

  })


})







//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/*
//Eliminar cuenta

app.delete('/apitechu/v5/cuentas/:_id', function(req, res) {  var _id = req.params._id

 clienteMlab = requestJson.createClient(urlMlabRapikeyaiz + "/cuentas/"+ _id + "?" + apiKey)

 clienteMlab.del('', function(err, resM, body) {

   res.sendStatus(resM.statusCode)

 })

}) (editado)
*/

// Da una cuenta disponible y la elimina para que no la pueda usar otro cliente
//http://localhost:3000/apitechu/v5/dameUnaCuentaDisponible
// IN -->
// OUT <-- La cuenta

app.get('/apitechu/v5/dameUnaCuentaDisponible', function(req,res){

  clientMlbab = requestJson.createClient(urlMlabRaiz+ "/cuentasDisponibles?"+apikey)
  clientMlbab.get('',function(err, resM, body){

      if (!err) {


        var cuentaDis = body[0].iban

        var _id = body[0]._id.$oid

        //Para borrar un elemento hay que pasar el id del mismo.

        clientMlbab = requestJson.createClient(urlMlabRaiz+ "/cuentasDisponibles/"+_id+ "?" + apikey)

        clientMlbab.del('', function(err, resM, body) {
          console.log(resM.statusCode)

        })

        res.send({"nuevaCuenta":cuentaDis}) //Retorna en Json la lista de usuarios, extraido de la base de datos en formato JSON
      }

  })


})


//Dar de alta una cuenta nueva asociada a un usuario
// Se creará con un importe inicial y un primer movmiento que es de la imposición incial
// http://localhost:3000/apitechu/v5/altaCuenta
// IN --> (headers) "iban":"ESXXXXXX" "idcliente":"23" "fecha":"aaaa/mm/dd" "importe":1000 "moneda":"EUR" "concepto":"Apertura de cuenta"
// OUT <-- Confirmación

app.post('/apitechu/v5/altaCuenta', function(req,res){



  if ((req.headers.iban=="")||(req.headers.idcliente=="")||(req.headers.fecha=="")||(req.headers.importe=="")||(req.headers.moneda==""))
  {
    res.status(404).send('faltan datos')
  }

console.log("importe: " + req.headers.importe);
console.log("moneda: " + req.headers.moneda );


  var jsonNuevaCuenta =
  {
    IBAN:req.headers.iban,
    idcliente: parseInt(req.headers.idcliente),
    movimientos:[{
                id: 1,
                importe: req.headers.importe,
                fecha: req.headers.fecha,
                moneda: req.headers.moneda,
                concepto:req.headers.concepto
                }],
    saldo:req.headers.importe

  }


  var query = 'q={"IBAN":' + '"' +req.headers.iban + '"' + '}'
  console.log(urlMlabRaiz+ "/cuentas?"+query + "&"+ apikey);
  clientMlbab = requestJson.createClient(urlMlabRaiz+ "/cuentas?"+query + "&"+ apikey)
  clientMlbab.get('',function(err, resM, body){
    console.log("Miro si existe la cuenta");
    if (!err)
    {

      if (body.length>=1)
      {
        console.log("la cuenta existe");
        res.status(404).send('Cuenta existe')

      }else {
            clientMlbab= requestJson.createClient(urlMlabRaiz+"/cuentas")

            clientMlbab.post("?"+apikey,jsonNuevaCuenta,function(err,resP,bodyP){
              res.send({"CuentaDadadeAlta":"ok"})
            })

      }
    }
   })

})



// Dar de alta un usuario
// http://localhost:3000/apitechu/v5/altaUsuario
// IN --> (headers)"id":"1" "nombre":"Eberto","apellido":"Birkinshaw","email":"ebirkinshaw1@fastcompany.com","password":"63OwdWmc","ciudad":"Mino","telefono":"182-481-2031"}
// OUT <-- Confirmación de insercción {"usuarioDadodeAlta":"ok","id": usuarioDadodeAlta }
//
app.post('/apitechu/v5/altaUsuario', function(req,res){


  if ((req.headers.id=="")||(req.headers.nombre=="")||(req.headers.apellido=="")||(req.headers.password==""))
  {
    res.status(404).send('faltan datos')
  }

  console.log("Todos los datos correctos ")
  // El id es como el DNI
  //He de encontrar un id valido, por lo que he de recorrer la lista y quedarme con el superior
  var ultimoId = req.headers.id

  //console.log("El body : " + JSON.stringify(req.body));
  console.log("El id : " + ultimoId);



  //genero ya el JSON valido


  var jsonNuevoUsuario =
  {
    id: parseInt(req.headers.id),
    nombre:req.headers.nombre,
    apellido:req.headers.apellido,
    email:req.headers.email,
    password:req.headers.password,
    ciudad:req.headers.ciudad,
    telefono:req.headers.telefono
  }

  console.log("Formo el JSON " + JSON.stringify(jsonNuevoUsuario) )

  //He de comporbar que no esta dado de alta ya

  var query = 'q={"email":' + '"' + req.headers.email + '"' + '}'
  console.log(urlMlabRaiz+ "/usuarios?"+query + "&"+ apikey);
  clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+query + "&"+ apikey)
  clientMlbab.get('',function(err, resM, body){
  console.log("Miro si el usuario existe");
    if (!err)
    {

      if (body.length>=1)
      {
        console.log("El usuario existe");
        res.status(404).send('Usuario existe')

      }else {
        console.log("El usuario no existe")

        clientMlbab= requestJson.createClient(urlMlabRaiz+"/usuarios")

        clientMlbab.post("?"+apikey,jsonNuevoUsuario,function(err,resP,bodyP){
          res.send({"usuarioDadodeAlta":"ok","id": ultimoId })
        })

      }
      //lo he encotrado, el usuario ya existe no se puede dar de alta

    }else {
      res.status(404).send('Error en consulta')
    }

  })


})


// Dar de alta un movimiento, modifica el saldo con el nuevo valor
// http://localhost:3000/apitechu/v5/altaMovimiento
// IN --> (headers) iban, id (liente) "fecha":"2017/03/01","importe":189.62,"moneda":"IDR","concepto":"Pago con tarjeta"}
// OUT <-- Confirmación de insercción
//
app.post('/apitechu/v5/altaMovimiento', function(req,res){

  var id = req.headers.id
  var iban=req.headers.iban
  var fecha = req.headers.fecha
  var importe = req.headers.importe
  var moneda = req.headers.moneda
  var concepto = req.headers.concepto

  var importeasumar = parseInt(importe)

  console.log("El importe a sumar al saldo  " + importeasumar );
  //Busco el usuario y cuenta
  //idcliente
  //IBAN
  var query = 'q={"idcliente":' + id + "," + '"IBAN":' + iban + '}'

  var fitroNombreAp = 'f={"_id":0,"movimientos":1,"saldo":1}'

  var cadenadebusqueda =  urlMlabRaiz+ "/cuentas?"+ query + "&"+ fitroNombreAp + "&"+ apikey

  clientMlbab = requestJson.createClient(cadenadebusqueda)

  clientMlbab.get('',function(err, resM, body){

    if (!err) {
      if (body.length == 1 )
      {

          //He formar el JSON del moviento
          clientMlbab= requestJson.createClient(urlMlabRaiz+"/cuentas")

          // he de encontrar el último id disponible
          var ultimomvt =  ultimoIDMovimietos(body[0].movimientos) // es el array de movientos

          var jsonNuevoMovimiento = {
            id:ultimomvt+1,
            fecha:fecha,
            importe:importe,
            moneda:moneda,
            concepto:concepto
          }

          var arrayAux = []

          for (var i = 0; i < body[0].movimientos.length; i++) {

            arrayAux[i] =  body[0].movimientos[i]

          }

          arrayAux[arrayAux.length] = jsonNuevoMovimiento

          console.log("El saldo antiguo " + body[0].saldo );
          var saldoNuevo = parseInt(body[0].saldo) +  importeasumar
          console.log("El nuevo saldo " + saldoNuevo );

          //var cambio = '{"$set":{"movimientos":' +  JSON.stringify(arrayAux) + "}}"

          var cambio = '{"$set":{"movimientos":' +  JSON.stringify(arrayAux) + "," + '"saldo":' + saldoNuevo + "}}"

         console.log("La cadena es " + cambio);

          //Se hace un put contra la api de MLAB para cambiar un valor del dato encontradp

          clientMlbab.put('?q={"idcliente":' + id + "," + '"IBAN":' + iban + '}&' + apikey,JSON.parse(cambio),function(err,resP,bodyP){
              res.send({"movimientoCargado":"ok"})
          })


      }else
      {
        res.status(404).send('Cuenta no encontrada')
      }

    }

  }) // end get

}) // end get

// lista de usuarios
// http://localhost:3000/apitechu/v5/usuarios
// se conecta con mlab para hacer una funcion get a la api y obtener los datos
// de la tabla usuarios
// IN -->
// OUT <-- lista de usuarios en formato JSON
app.get('/apitechu/v5/usuarios', function(req,res){
  clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+apikey)
  clientMlbab.get('',function(err, resM, body){

      if (!err) {
        res.send(body) //Retorna en Json la lista de usuarios, extraido de la base de datos en formato JSON
      }

  })


})

//Devolver nombre y apellido de un solo usuario
//http://localhost:3000/apitechu/v5/usuarios/:id
// IN --> (parametro) identificador del usuario
// OUT <-- JSON con usuario que cumple el id (dato sin filtar)
app.get('/apitechu/v5/usuarios/:id', function(req,res){
  var id = req.params.id

  var query = 'q={"id":' + id + '}'
  clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + apikey)
  //clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&" + apikey)
  clientMlbab.get('',function(err, resM, body){

      if (!err) {
        if (body.length >0)
        {
          res.send(body[0]) //Retorna en Json la lista de usuarios, extraido de la base de datos en formato JSON
        }else {
          res.status(404).send('usuario no encontrado')
        }

      }

  })

})

/// Igual que el anterior pero filtrando por Nombre y apellido
//http://localhost:3000/apitechu/v5/usuariosConfiltro/:id
// IN --> (parametro) identificador del usuario
// OUT <-- JSON con usuario que cumple el id, retorna (nombre, apellido, password, loged)
app.get('/apitechu/v5/usuariosConfiltro/:id', function(req,res){
  var id = req.params.id

  var query = 'q={"id":' + id + '}'
  var fitroNombreAp = 'f={"_id":0,"nombre":1,"apellido":1,"email":1,"password":1,"loged":1}'
  clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + fitroNombreAp +  "&"+ apikey)
  //clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&" + apikey)
  clientMlbab.get('',function(err, resM, body){

      if (!err) {
        if (body.length >0)
        {
          res.send(body[0]) //Retorna en Json la lista de usuarios, extraido de la base de datos en formato JSON
        }else {
          res.status(404).send('usuario no encontrado')
        }

      }

  })

})

/// login con mongo (mlab)
//  ----- ejemplo para ver como se añade un campo  ----
//  http://localhost:3000/apitechu/v5/login
//  IN --> (headers) password, email
//  out <-- añade un campo de loged=true, retorna el id y nombre
app.post('/apitechu/v5/login', function(req,res,next){
  var password = req.headers.password
  var email=req.headers.email
  console.log("con cors")
  console.log(email)

  var cadenaComilla = ","
  var cadenapass = '"password":'

  var query = 'q={"email":' + email + cadenaComilla + cadenapass + password + '}'

  console.log(query)

  var fitroNombreAp = 'f={"_id":0,"id":1,"nombre":1,"apellido":1,"password":1}'

  var cadenadebusqueda =  urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + fitroNombreAp +  "&"+ apikey

  console.log(cadenadebusqueda)

  clientMlbab = requestJson.createClient(cadenadebusqueda)
  //clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&" + apikey)
  clientMlbab.get('',function(err, resM, body){

      if (!err) {
        console.log("Busco login")
        console.log(body)
        if (body.length == 1 )
        {

            console.log("login correcto")
            clientMlbab= requestJson.createClient(urlMlabRaiz+"/usuarios")

            var cambio = '{"$set":{"loged":true}}'
            console.log(cambio)
            //Se hace un put contra la api de MLAB para cambiar un valor del dato encontradp
            console.log('?q={"id"' + ":" + body[0].id +'}&' + apikey,JSON.parse(cambio))
            clientMlbab.put('?q={"id"' + ":" + body[0].id +'}&' + apikey,JSON.parse(cambio),function(err,resP,bodyP){
                res.send({"login":"ok","id":body[0].id,"nombre":body[0].nombre}) //Retorna en Json la lista de usuarios, extraido de la base de datos en formato JSON
            })


        }else
        {
          console.log("login no correcto")
          res.status(404).send('Usuario no encontrado')
        }

      }

  })

})

//// logout de mongo (mlab)
//   http://localhost:3000/apitechu/v5/logout
//   IN --> (headers) id
//   OUT <-- pone campo loged=false, retorna el id
app.post('/apitechu/v5/logout', function(req,res){
  var id = req.headers.id

  var query = 'q={"id":' + id + ', "loged":true }'
  //var query = ""

  var fitroNombreAp = 'f={"_id":0,"id":1,"nombre":1,"apellido":1,"password":1}'

  var cadenadebusqueda =  urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + fitroNombreAp +  "&"+ apikey

  clientMlbab = requestJson.createClient(cadenadebusqueda)
  //clientMlbab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&" + apikey)
  clientMlbab.get('',function(err, resM, body){

      if (!err) {
        console.log("Busco login")
        console.log(body)
        if (body.length == 1 )
        {

            console.log("Estaba logado")
            clientMlbab= requestJson.createClient(urlMlabRaiz+"/usuarios")

            var cambio = '{"$set":{"loged":false}}'
            console.log(cambio)
            //Se hace un put contra la api de MLAB para cambiar un valor del dato encontradp
            console.log('?q={"id"' + ":" + body[0].id +'}&' + apikey,JSON.parse(cambio))
            clientMlbab.put('?q={"id"' + ":" + body[0].id +'}&' + apikey,JSON.parse(cambio),function(err,resP,bodyP){
                res.send({"logout":"ok","id":body[0].id }) //Retorna en Json la lista de usuarios, extraido de la base de datos en formato JSON
            })


        }else
        {
          res.status(200).send('Usuario no logado')
        }

      }

  })

})

//// Consulta de las cuentas de un cliente
//  http://localhost:3000/apitechu/v5/cuentasdeCliente
//  IN --> (headers) id
//  OUT <-- cuentas (IBAN) de un cliente en formato JSON
app.post('/apitechu/v5/cuentasdeCliente', function(req,res){

  console.log("Estoy en las cuentas del cliente");
  var id = req.headers.id

  console.log("El id recibido " +  id );

  var query = 'q={"idcliente":' + id + '}'


  var fitroNombreAp = 'f={"_id":0,"IBAN":1,"saldo":1}'

  var cadenadebusqueda =  urlMlabRaiz+ "/cuentas?"+ query + "&" +fitroNombreAp +  "&"+ apikey

  console.log("La cadena de busqueda " +  cadenadebusqueda );

  clientMlbab = requestJson.createClient(cadenadebusqueda)

  clientMlbab.get('',function(err, resM, body){
      if (!err) {
        if (body.length>0)
        {

            //var cadenaIBANES = null
            //for (var i = 0; i < body.length; i++) {
            //      cadenaIBANES = '"IBAN' + i + '""' + ":" + '"' + body[i].IBAN '"'
            //}
              console.log("Paso los datos " + body );

          res.send(body)
      }else {


        console.log("no encuentro cuentas");
        res.status(200).send('Usuario sin cuentas')
      }


      }else {

      }

  })
})//fin post

/// Consulta de movimientos de una cuenta (IBAN)
// llamada local
// http://localhost:3000/apitechu/v5/movimientosCuenta
// llamada a openshift (desde postman)
// http://rutaapi-apitechupfm.7e14.starter-us-west-2.openshiftapps.com/apitechu/v5/movimientosCuenta
// IN --> (headers) iban
// OUT <-- movimientos filtando solo con el dato movimientos en formato JSON
app.post('/apitechu/v5/movimientosCuenta', function(req,res){
  console.log("Entro en movimientos");
  var iban = req.headers.iban

  var query = 'q={"IBAN":' + iban + '}'

  var fitroNombreAp = 'f={"_id":0,"movimientos":1}'

  //var ordenacion = 's={"movimientos.importe":-1}' // Esto no funciona

  var cadenadebusqueda =  urlMlabRaiz+ "/cuentas?"+ query + "&" +fitroNombreAp +  "&"+ apikey
  //var cadenadebusqueda =  urlMlabRaiz+ "/cuentas?"+ query + "&" + ordenacion + "&" +fitroNombreAp + "&"+ apikey

  console.log(cadenadebusqueda);

  clientMlbab = requestJson.createClient(cadenadebusqueda)

  clientMlbab.get('',function(err, resM, body){

      if (!err) {
        if (body.length>0)
        {
          console.log("Hay movimientos");
          console.log(body);
          res.send(body[0].movimientos)
        }else {
          console.log("no hay movimientos");
          res.status(400).send('cuentas sin movimientos')
        }
      }

  })


})//Fin get


////////// FIN Con Mongo  /////////////////

////// Usando ficheros //////////////////////////

var cuentas = require ('./cuentas.json')

//Se obtienen las cuentas del fichero cuentas.json
// http://localhost:3000/apitechu/v1/cuentas
// IN -->
// OUT <-- cuentas formato JSON
app.get('/apitechu/v1/cuentas',function(req,res){
  //res.sendfile('usuarios.json')
  res.send(cuentas)
})


//dada una numero de cuenta dar los movimientos del fichero cuentas.json
// http://localhost:3000/apitechu/v1/cuentas/movimientos/
// IN --> (headers) iban (se debe pasar entre "", como string)
// OUT <-- cuentas en formato JSON extraidas del fichero cuentas.json
app.post('/apitechu/v1/cuentas/movimientos/',function(req,res){
  //console.log(req.headers)
  var iban = req.headers.iban
  console.log(iban)
  var arraydecuentas = []
  var entradoIban = false
  for (var i = 0; i <cuentas.length; i++) {
     //console.log(cuentas[i].IBAN)

     if (cuentas[i].IBAN == iban ) {
       console.log("Encontrado ")
       entradoIban = true
       for (var j = 0; j < cuentas[i].movimientos.length; j++) {
         arraydecuentas[j]=cuentas[i].movimientos[j]

         //console.log(cuentas[i].movimientos[j])
         console.log(arraydecuentas[j])

       }

     } else {

     }

  }
  if (entradoIban)
  {res.send(JSON.stringify(arraydecuentas))}
  else {
    res.sed("Iban dado no es valido")
  }

})


//dado un usuario me da sus cuentas
//  http://localhost:3000/apitechu/v1/cuentas/cuentasCliente/
//  IN --> (headers) idcliente
//  OUT <-- cuentas en formato JSON extraidas de fiechero clientes.json
app.post('/apitechu/v1/cuentas/cuentasCliente/',function(req,res){
  console.log("Pido cuentas")
  var cliente = req.headers.idcliente
  console.log(cliente)
  var listaDeCuentas = []
  var contadordecuentas = 0
  var nuevo
  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].idcliente == cliente) {
      //listaDeCuentas[contadordecuentas]=cuentas[i].IBAN

      //nuevo ={"numero":contadordecuentas,
      //            "cuenta":cuentas[i].IBAN}
      nuevo ={"cuenta":cuentas[i].IBAN}
      listaDeCuentas[contadordecuentas]=nuevo
      //listaDeCuentas.push(["cuenta " + contadordecuentas, cuentas[i].IBAN]);
      contadordecuentas++
    }
  }

  if (contadordecuentas>0)
  {
    //res.send(listaDeCuentas)
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(listaDeCuentas))
    //res.send('{"nombe":"pedro"}')

  }else {
    res.send("Usuario no encontrado")
  }

})


////// Usando ficheros //////////////////////////

////////GET ///////
// Función que hace el hola mundo
// http://localhost:3000//apitechu/v1
// IN -->
// OUT <-- mensaje de Bienvenida
app.get('/apitechu/v1',function(req,res){
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})

// Da la lista de usuarios extradidas de un fichero
// http://localhost:3000//apitechu/v1/usuarios
// IN -->
// OUT <-- lista de usuarios, se trata del fichero usuarios.json
app.get('/apitechu/v1/usuarios',function(req,res){
  //res.sendfile('usuarios.json')
  res.send(usuarios)
})

/////////POST/////////
// Añade un nuevo dato a la lista de usuarios del fichero, los parametros se pasa por headers //
app.post('/apitechu/v1/usuarios',function(req,res){
  var nuevo ={"first_name":req.headers.first_name,
              "country":req.headers.country
            }
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios) // Convierte el JSON en cadena de caracteres

  fs.writeFile("./usuarios-2.json",datos,"utf8",function(err){

    if (err)
    {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }

  }) //Escribe en el fiechero el resultado

  res.send("Alta ok")

})

/////////////////Usando el body
// Añade un nuevo dato a la lista de usuarios del fichero, los parametros se pasa por body //
app.post('/apitechu/v2/usuarios',function(req,res){

  var nuevo = req.bodyParser

  usuarios.push(nuevo)
  //console.log(req.headers)
  const datos = JSON.stringify(usuarios) // Convierte el JSON en cadena de caracteres

  fs.writeFile("./usuarios-2.json",datos,"utf8",function(err){

    if (err)
    {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }

  }) //Escribe en el fiechero el resultado

  res.send("Alta ok")

})

//////// DELETE ///////
// Borra un dato de la lista de usuarios, se pasa por parametro //
app.delete('/apitechu/v1/usuarios/:id',function(req,res){
  usuarios.splice(req.params.id-1,1)
  res.send("usuario borrado")
})


//Demo para imprimir los datos que se pasan a una api
// parametros    --> /:p1/:p2 ....
// Query string  --> /?------
// headers       --> cabeceras  parametro1=valor1 ....
// body          --> valores ....
app.post('/apitechu/v1/mostruo/:p1/:p2',function(req,res){

  console.log("parametros")
  console.log(req.params)

  console.log("quiery string")
  console.log(req.query)

  console.log("headers")
  console.log(req.headers)

  console.log("body")
  console.log(req.body)

})

/// hago get con la v3
// Lista de usuarios del fichero, con otra versión
app.get('/apitechu/v3/usuarios',function(req,res){
  //res.sendfile('usuarios.json')
  res.send(usuarios)
})


/////////POST/////////
// operación de login usando fichero //
app.post('/apitechu/v3/usuarios/login/',function(req,res){

  var password = req.headers.password
  var email=req.headers.email

  console.log(password)
  console.log(email)

  var idusuario = 0

   for (var i = 0; i < usuarios.length; i++) {
     if( usuarios[i].email == email && usuarios[i].password == password)
     {
         idusuario = usuarios[i].id
         usuarios[i].logged = true
         usuarios.push()
         break
     }
   }
   if (idusuario != 0)
   {
     res.send({"encontrado":"si","id":idusuario})
   }else {
     res.send({"encontrado":"no"})
   }

})

/////////POST/////////
// logout de usuario usando Fichero //
app.post('/apitechu/v3/usuarios/logout/',function(req,res){
  var id = req.headers.id
  var logado = false
  console.log(id)
  for (var i = 0; i < usuarios.length; i++) {
    if( (usuarios[i].id == id) &&  (usuarios[i].logged == true))
    {
        console.log(usuarios[i].id)
        usuarios[i].logged = false
        usuarios.push()
        res.send({"logout":"si"})
        logado = true
        break
    }
  }

  if (!logado)
  {
    res.send({"logout":"no encontrado"})
  }



})
